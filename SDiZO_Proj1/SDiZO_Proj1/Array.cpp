#include "stdafx.h"
#include "Array.h"


Array::Array()
{
	start = NULL;
}


Array::~Array()
{
	delete[elements] start;
	elements = 0;
	start = NULL;
}

void Array::add(int value)
{
	//Utworzenie nowej tablicy
	int* new_array = new int[elements + 1];
	//Przepisanie element�w ze starej tablicy
	//W przypadku braku element�w nie zostanie przeniesione nic
	for(unsigned int i = 0; i < elements; ++i){
		new_array[i] = start[i];
	}
	//Dodanie warto�ci na ko�cu tablicy + inkrementacja rozmiaru
	new_array[elements++] = value;
	//Ustawienie wska�nika na now� tablic�
	delete[] start;
	start = new_array;
}

void Array::insert(int pos, int value)
{
	//Jesli indeks niepoprawny zwrocenie wyjatku
	if ((pos < 0) || ((unsigned int)pos > elements)) throw std::invalid_argument("Indeks niepoprawny");
	//Gdy brak tablicy -> utworzenie jej
	if (elements == 0) {
		start = new int[1];
		start[0] = value;
		++elements;
		return;
	}
	++elements;
	//Utworzenie nowej tablicy o rozmiarze o 1 wiekszym niz aktualnej
	int* new_array = new int[elements];
	for (int i = 0; i < pos; ++i) {
		//Przepisanie wartosci ze starej tablicy
		new_array[i] = start[i];
	}
	//Dodanie elementu
	new_array[pos] = value;
	for (unsigned int i = pos + 1; i <= elements; ++i) {
		//Przepisanie wartosci ze starej tablicy
		new_array[i] = start[i-1];
	}
	//Usuniecie starej tablicy
	delete[] start;
	//Zmiana wskazania na nowa tablice
	start = new_array;
}

void Array::remove(int number)
{
	//Gdy indeks poza tablica -> zwrocenie wyjatku
	if (((unsigned int)number >= elements) || number < 0) throw std::out_of_range("Indeks wykracza poza tablice!");
	//Utworzenie tablicy, gdy brak
	if (elements == 0) {
		delete[] start;
		start = NULL;
		return;
	}
	--elements;
	//Utworzenie nowej tablicy o rozmiarze o 1 mniejszym niz aktualnej
	int* new_array = new int[elements];
	for (int i = 0; i < number; ++i) {
		//Przepisanie wartosci ze starej tablicy
		new_array[i] = start[i];
	}
	for (unsigned int i = number + 1; i <= elements; ++i) {
		//Przepisanie wartosci ze starej tablicy
		new_array[i - 1] = start[i];
	}
	//Usuniecie starej tablicy
	delete[] start;
	//Zmiana wskazania na nowa tablice
	start = new_array;
}

void Array::removeAll()
{
	//Gdy tablica istnieje -> usuniecie jej
	if (start != NULL) {
		delete[] start;
		start = NULL;
		elements = 0;
	}
}

bool Array::exist(void* i, int value)
{
	//Iteracja po elementach, porownywanie wartosci
	for (unsigned int i = 0; i < elements; ++i) if (start[i] == value) return true;
	return false;
}

std::string Array::toString(void* i)
{
	std::string out = "[";
	//Iteracja po elementach, dopisywanie wartosci
	if (i == NULL)	for (unsigned int x = 0; x < elements; ++x) {
						out += (std::to_string(start[x]) + ",");
					}
	out.pop_back();
	out += "]";
	return out;
}

void Array::change_order(unsigned int a, unsigned int b)
{
	//Zamiana kluczy przy pomocy tymczasowej zmiennej
	unsigned int temp = start[a];
	start[a] = start[b];
	start[b] = temp;
}

int Array::getLast()
{
	//Jesli tablica istnieje zwraca wartosc ostatniego elementu
	if(elements) return start[elements-1];
	return 0;
}
