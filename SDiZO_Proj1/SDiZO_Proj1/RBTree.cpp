#include "stdafx.h"
#include "RBTree.h"


RBTree::RBTree()
{
	root = NIL;
}


RBTree::~RBTree()
{
	root = NIL;
}

void RBTree::add(int value)
{
	//Dodaje element jak do drzewa BST
	element** pointer = &root;
	element* parent = NIL;
	while (*pointer != NIL) {
		parent = *pointer;
		if (value < (*pointer)->key) pointer = &((*pointer)->left);
		else pointer = &((*pointer)->right);
	}
	//Tworzy nowy element
	(*pointer) = new element();
	//Dodaje klucz
	(*pointer)->key = value;
	//Ustawia wskazanie na ojca
	(*pointer)->parent = parent;
	//Ustawia wskazanie na lewego syna
	(*pointer)->left = NIL;
	//Ustawia wskazanie na prawego syna
	(*pointer)->right = NIL;
	//Ustawia kolor
	(*pointer)->color = RED;

	//Naprawia wlasnosci drzewa czerwono-czarnego
	restore_add(*pointer);
}

void RBTree::insert(int pos, int value)
{
	//Dodaje jako kolejny element, uzywa add()
	add(value);
}

void RBTree::remove(int number)
{
	//Szuka elementu o podanej wartosci
	element* pointer = find(NULL, number);
	//Jesli nie znaleziono -> zwraca wyjatek
	if (pointer == NULL) throw new std::invalid_argument("Nie ma takiego klucza w strukturze.");
	//Usuwa element podajac wskazanie na niego
	remove(pointer);
}

void RBTree::remove(element * pointer)
{
	//Ustawia wskazanie na ojca
	element* parent = pointer->parent;
	//Sprawdza kolejne przypadki
	if (pointer->left == NIL) {
		//Nie ma dzieci
		if (pointer->right == NIL) {
			//Element jest korzeniem
			if (parent == NIL) {
				root = NIL;
				return;
			}
			//Element jest lewym synem ojca
			if (parent->left == pointer) {
				parent->left = NIL;
				return;
			}
			//Element jest prawym synem ojca
			parent->right = NIL;
		}
		//Jest tylko prawy syn
		else {
			//Element jest korzeniem
			if (parent == NIL) {
				root = pointer->right;
				root->parent = NIL;
				//Przywraca wlasnosci drzewa czerwono-czarnego
				restore_remove(root);
				return;
			}
			//Element jest lewym synem ojca
			if (parent->left == pointer) {
				parent->left = pointer->right;
				parent->left->parent = parent;
				//Przywraca wlasnosci drzewa czerwono-czarnego
				restore_remove(parent->left);
				return;
			}
			//Element jest prawym synem ojca
			parent->right = pointer->right;
			parent->right->parent = parent;
			//Przywraca wlasnosci drzewa czerwono-czarnego
			restore_remove(parent->right);
		}
	}
	else {
		//Jest tylko lewy syn
		if (pointer->right == NIL) {
			//Element jest korzeniem
			if (parent == NIL) {
				root = pointer->left;
				root->parent = NIL;
				//Przywraca wlasnosci drzewa czerwono-czarnego
				restore_remove(root);
				return;
			}
			//Element jest lewym synem ojca
			if (parent->left == pointer) {
				parent->left = pointer->left;
				parent->left->parent = parent;
				//Przywraca wlasnosci drzewa czerwono-czarnego
				restore_remove(parent->left);
				return;
			}
			//Element jest prawym synem ojca
			parent->right = pointer->left;
			parent->right->parent = parent;
			//Przywraca wlasnosci drzewa czerwono-czarnego
			restore_remove(parent->right);
		}
		//Obydwoje synow
		else {
			if ((std::rand() % 1) >= 0.5) {
				element* next = successor(pointer);
				pointer->key = next->key;
				remove(next);
			}
			else {
				element* previous = predecessor(pointer);
				pointer->key = previous->key;
				remove(previous);
			}
			//Przywraca wlasnosci drzewa czerwono-czarnego
			restore_remove(pointer);
		}
	}
}
void RBTree::destroy(element* pointer)
{
	//Jesli istnieje lewy syn -> usuwa go rekurencyjnie
	if (pointer->left != NIL) {
		destroy(pointer->left);
		pointer->left = NIL;
	}
	//Jesli istnieje prawy syn -> usuwa go rekurencyjnie
	if (pointer->right != NIL) {
		destroy(pointer->right);
		pointer->right = NIL;
	}
	delete pointer;
}

void RBTree::removeAll()
{
	//Jesli istnieje drzewo -> usuwa je
	if (root != NIL) {
		destroy(root);
		root = NIL;
		elements = 0;
	}
}

bool RBTree::exist(void * i, int value)
{
	//Ustawia wartosc poczatkowa
	element* pointer = (element*)i;
	if (i == NULL) pointer = root;
	if (pointer == NIL) return false;
	//Sprawdz rekurencyjnie czy wartosc istnieje
	return (value == pointer->key)
		|| ((value < pointer->key) && exist(pointer->left, value))
		|| ((value > pointer->key) && exist(pointer->right, value));	
}

std::string RBTree::toString(void * i)
{
	element* pointer;
	//Ustawia wartosc poczatkowa
	if (i == NULL) pointer = root;
	else pointer = (element*)i;
	if (pointer == NIL) return "";
	std::string out = "\nPOINT | P: ";
	//Dodaje klucz ojca
	if (pointer->parent != NIL) out += std::to_string(pointer->parent->key);
	else out += "NULL";
	//Dodaje klucz elementu
	out += (" | key: " + std::to_string(pointer->key)) + " | color: ";
	//Dodaje kolor elementu
	if (pointer->color == RED) out += "RED";
	else out += "BLACK";
	out += " | L: ";
	//Dodaje klucz lewego syna
	if (pointer->left != NIL) out += std::to_string(pointer->left->key);
	else out += "NULL";
	out += " | R: ";
	//Dodaje prawego syna
	if (pointer->right != NIL) out += std::to_string(pointer->right->key);
	else out += "NULL";
	//Dodaje reprezentacje prawego poddrzewa
	if (pointer->left != NIL) out += (toString(pointer->left));
	//Dodaje reprezentacje lewego poddrzewa
	if (pointer->right != NIL) out += (toString(pointer->right));
	return out;
}

int RBTree::getLast()
{
	//Sprawdza czy drzewo istnieje
	if (root == NULL) return 0;
	element* pointer = root;
	//Porusza sie po prawych synach, az do konca
	if (root->right == NULL) return root->key;
	while (pointer->right != NULL) pointer = pointer->right;
	return pointer->key;
}

RBTree::element * RBTree::find(element * i, int value)
{
	element* pointer;
	//Ustawia wartosc poczatkowa
	if (i == NULL) pointer = root;
	else pointer = (element*)i;
	//Jesli wskaznik wskazuje na element NIL -> zwracany NULL
	if (pointer == NIL) return NULL;
	//Jesli wskaznik wskazuje na element o podanej wartosci -> zwracany wskaznik na wskaznik
	if (pointer->key == value) return pointer;
	element* search;
	//Rekurencyjnie poszukiwanie lewym i prawym poddrzewie
	if ((search = find(pointer->left, value)) != NULL) return search;
	if ((search = find(pointer->right, value)) != NULL) return search;
	return NULL;
}

void RBTree::restore_add(element * pointer)
{
	//Przypadek 0
	if (pointer->parent == NIL) {
		pointer->color = BLACK;
		return;
	}
	//Przypadek 1
	if (pointer->parent->color == BLACK) return;
	element* pointer2 = pointer->parent->parent;
	//Jest dziadek
		//Element jest w lewym poddrzewie dziadka
		if (pointer2->left == pointer->parent) {
			//Stryj w prawym poddrzewie
				//Kolor stryja - czerwony
				if (pointer2->right->color == RED) {
					pointer->parent->color = BLACK;
					pointer2->right->color = BLACK;
					pointer2->color = RED;
					restore_add(pointer2);
					return;
				}
				//Kolor stryja - czarny
				//Element w prawym poddrzewie ojca
				if (pointer->parent->right == pointer) {
					rot_L(pointer->parent);
					restore_add(pointer->left);
					return;
				}
				//Element w lewym poddrzewie ojca
				if (pointer->parent->left == pointer) {
					rot_R(pointer2);
					pointer->parent->color = !(pointer->parent->color);
					pointer2->color = !(pointer2->color);
				}
		}
		//Element jest w prawym poddrzewie dziadka
		else {
			//Stryj w lewym poddrzewie
				//Kolor stryja - czerwony
				if (pointer2->left->color == RED) {
					pointer->parent->color = BLACK;
					pointer2->left->color = BLACK;
					pointer2->color = RED;
					restore_add(pointer2);
					return;
				}
				//Kolor stryja - czarny
				//Element w lewym poddrzewie ojca
				if (pointer->parent->left == pointer) {
					rot_R(pointer->parent);
					restore_add(pointer->right);
					return;
				}
				//Element w prawym poddrzewie ojca
				if (pointer->parent->right == pointer) {
					rot_L(pointer2);
					pointer->parent->color = !(pointer->parent->color);
					pointer2->color = !(pointer2->color);
				}
		}
}

void RBTree::restore_remove(element * pointer)
{
	//Sprawdza kolejne przypadki w celu naprawienia wlasnosci drzewa czerwono-czarnego
	element* w;
	if (pointer->color == RED) return;
	while ((pointer->color == BLACK) && (pointer->parent != NIL))
	{
		if (pointer->parent->left == pointer) {
			w = pointer->parent->right;
			if (w->color == RED) {
				w->color = BLACK;
				pointer->parent->color = RED;
				rot_L(pointer->parent);
				w = pointer->parent->right;
			}
			if ((w->left->color == BLACK) && (w->right->color == BLACK)) {
				w->color = RED;
				pointer = pointer->parent;
			}
			else {
				if (w->right->color == BLACK) {
					w->left->color = BLACK;
					w->color = RED;
					rot_R(w);
					w = pointer->parent->right;
				}
				w->color = pointer->parent->color;
				pointer->parent->color = BLACK;
				w->right->color = BLACK;
				rot_L(pointer->parent);
				pointer = root;
			}
		}
		else {
			w = pointer->parent->left;
			if (w->color == RED) {
				w->color = BLACK;
				pointer->parent->color = RED;
				rot_R(pointer->parent);
				w = pointer->parent->left;
			}
			if ((w->left->color == BLACK) && (w->right->color == BLACK)) {
				w->color = RED;
				pointer = pointer->parent;
			}
			else {
				if (w->left->color == BLACK) {
					w->right->color = BLACK;
					w->color = RED;
					rot_L(w);
					w = pointer->parent->left;
				}
				w->color = pointer->parent->color;
				pointer->parent->color = BLACK;
				w->left->color = BLACK;
				rot_R(pointer->parent);
				pointer = root;
			}
		}
	}
	pointer->color = BLACK;
}

void RBTree::rot_L(element * pointer)
{
	//Rotuje w lewo wokol podanego elementu
	element* child = pointer->right;
	element* parent = pointer->parent;
	pointer->right = child->left;
	if (pointer->right != NIL) pointer->right->parent = pointer;
	child->left = pointer;
	pointer->parent = child;
	child->parent = parent;
	if (parent != NIL)
	{
		if (parent->left == pointer) parent->left = child;
		else parent->right = child;
	}
	else root = child;
}

void RBTree::rot_R(element * pointer)
{
	//Rotuje w prawo wokol podanego elementu
	element* child = pointer->left;
	element* parent = pointer->parent;
	pointer->left = child->right;
	if (pointer->left != NIL) pointer->left->parent = pointer;
	child->right = pointer;
	pointer->parent = child;
	child->parent = parent;
	if (parent != NIL)
	{
		if (parent->left == pointer) parent->left = child;
		else parent->right = child;
	}
	else root = child;
}

RBTree::element * RBTree::predecessor(RBTree::element * pointer)
{
	//Poszukuje poprzednika w lewym poddrzewie
	if (pointer->left != NIL) {
		pointer = pointer->left;
		while (pointer->right != NIL) pointer = pointer->right;
		return pointer;
	}
	//Poszukuje poprzednika w lewych poddrzewach przodkow
	while (pointer->parent != NIL) {
		if (pointer->parent->right == pointer) return pointer->parent;
		pointer = pointer->parent;
	}
	return NIL;
}

RBTree::element * RBTree::successor(RBTree::element * pointer)
{
	//Poszukuje poprzednika w prawym poddrzewie
	if (pointer->right != NIL) {
		pointer = pointer->right;
		while (pointer->left != NIL) pointer = pointer->left;
		return pointer;
	}
	//Poszukuje poprzednika w prawych poddrzewach przodkow
	while (pointer->parent != NIL) {
		if (pointer->parent->left == pointer) return pointer->left;
		pointer = pointer->parent;
	}
	return NIL;
}
