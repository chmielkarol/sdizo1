#pragma once
#include "Data_struct.h"

class Array :
	public Data_struct
{
public:
	Array();
	~Array();

	//Dodaje element na koniec tablicy
	void add(int value) override;

	//Dodaje element w wyznaczonym miejscu (indeksie) tablicy
	void insert(int pos, int value) override;

	//Usuwa element o podanym indeksie
	void remove(int number) override;

	//Usuwa wszystkie elementy z tablicy
	void removeAll() override;

	//Zwraca czy podana wartosc wystepuje w tablicy
	bool exist(void* i, int value) override;

	//Zwraca odwzorowanie struktury jako string
	std::string toString(void*) override;

	//Zwraca ostatni element
	int getLast() override;

private:
	//Wska�nik na poczatek tablicy
	int* start;
	//Zamienia 2 elementy miejscami
	void change_order(unsigned int a, unsigned int b);
};

