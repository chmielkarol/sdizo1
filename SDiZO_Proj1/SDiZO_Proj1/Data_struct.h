#pragma once

class Data_struct
{
public:
	Data_struct();
	virtual ~Data_struct();

	//DODAWANIE DO STRUKTURY

		//Dodaj
		//- na ko�cu tablicy
		//- na ko�cu listy
		//- do kopca binarnego, drzewa czerwono-czarnego, drzewa AVL
		virtual void add(int value);

		//Dodaj
		//- na wybranej pozycji w tablicy
		//- po podanej wartosci w liscie
		virtual void insert(int pos, int value);

		//Dodaj z pliku
		void add_file(std::string path);

	////////////////////////

	//USUWANIE ZE STRUKTURY

		//Z podanego indeksu tablicy
		//Podanej warto�ci listy, kopca binarnego, drzewa czerwono-czarnego, drzewa AVL
		virtual void remove(int number);
		//Wszystkich element�w
		virtual void removeAll();

	////////////////////////

	//WYSZUKIWANIE W STRUKTURZE
	
		//Zaczynajac od podanego elementu wyszukuje element w strukturze
		virtual bool exist(void* i, int value);

	////////////////////////

	//WY�WIETLANIE STRUKTURY OD DANEGO ELEMENTU

		virtual std::string toString(void* i);

	////////////////////////

	//WY�WIETLANIE LICZBY ELEMENTOW

		unsigned int getElements();

	////////////////////////

	//WY�WIETLANIE OSTATNIEGO ELEMENTU

		virtual int getLast();

	////////////////////////


protected:
	//Liczba element�w struktury
		unsigned int elements;
};

