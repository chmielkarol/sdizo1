#pragma once
#include "Data_struct.h"
#include <list>

class STLList :
	public Data_struct
{
public:
	STLList();
	~STLList();

	//Dodaje element na koniec listy
	void add(int value) override;

	//Dodaje element po elemencie o podanej wartosci
	void insert(int pos, int value) override;

	//Usuwa element o podanej wartosci
	void remove(int number) override;

	//Usuwa wszystkie elementy
	void removeAll() override;

	//Zwraca czy element o podanej wartosci istnieje w liscie
	bool exist(void* i, int value) override;

	//Zwraca reprezentacje listy jako string
	std::string toString(void* i) override;

	//Zwraca wskazanie na ostatni element
	int getLast() override;

private:
	//Obiekt listy STL
	std::list<int> data;
};

