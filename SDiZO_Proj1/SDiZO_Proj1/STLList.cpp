#include "stdafx.h"
#include "STLList.h"


STLList::STLList()
{
}


STLList::~STLList()
{
	data.clear();
}

void STLList::add(int value)
{
	//Dodaje element na koncu listy przy pomocy metody push_back()
	data.push_back(value);
}

void STLList::insert(int pos, int value)
{
	//Znajduje pozycje elementu o podanej wartosci
	std::list<int>::iterator i = std::find(data.begin(), data.end(), pos);
	//Dodaje element na podanej pozycji
	if (i == data.end()) data.push_front(value);
	else data.insert(++i, value);
}

void STLList::remove(int number)
{
	//Usuwa element o podanej wartosci
	data.remove(number);
}

void STLList::removeAll()
{
	//Usuwa liste
	data.clear();
}

bool STLList::exist(void* i, int value)
{
	//Jesli znaleziono element o podanej wartosci zwraca TRUE
	return (std::find(data.begin(), data.end(), value) != data.end());
}

std::string STLList::toString(void* i)
{
	std::string out = "";
	//Dopisuje kolejne elementy listy
	for (std::list<int>::iterator i = data.begin(); i != data.end(); ++i) out += '[' + std::to_string(*i) + "] -> ";
	out += "NULL";
	return out;
}

int STLList::getLast()
{
	//Zwraca ostatni element listy przy pomocy metody back()
	if (data.size()) return data.back();
	return 0;
}
