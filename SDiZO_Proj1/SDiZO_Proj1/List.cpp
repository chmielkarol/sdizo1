#include "stdafx.h"
#include "List.h"
#include <string>

List::List()
{
	start = NULL;
	last = NULL;
}


List::~List()
{
	start = NULL;
	last = NULL;
}

void List::add(int value)
{
	//Je�li lista pusta - utworzenie pierwszego elementu
	if (start == NULL) {
		//Przypisanie warto�ci
		start = new element();
		start->value = value;
		start->next = NULL;
		last = start;
	}
	else {
		//Pod��czenie nowego elementu do poprzednio ostatniego
		last->next = new element();
		last = last->next;
		//Przypisanie warto�ci
		last->value = value;
		last->next = NULL;
	}
	++elements;
}

void List::insert(int pos, int value)
{
	//Je�li lista pusta - utworzenie pierwszego elementu
	if (start == NULL) {
		//Przypisanie warto�ci
		start = new element();
		start->value = value;
		start->next = NULL;
		last = start;
	}
	//W przeciwnym razie poszukiwanie elementu o podanej wartosci
	else {
		bool found = false;
		element* pointer = start;
		while (pointer != NULL) {
			//Zatrzymanie gdy znaleziono
			if ((pointer->value) == pos)
			{
				found = true;
				break;
			}
			pointer = pointer->next;
		}
		//Jesli znaleziono element
		if (found) {
			//Dodanie elementu i polaczenie kolejnych
			element* next = pointer->next;
			pointer->next = new element();
			pointer = pointer->next;
			pointer->value = value;
			pointer->next = next;
		}
		//Jesli nie znaleziono elementu
		else {
			element* next = start;
			start = new element();
			start->value = value;
			start->next = next;
		}
	}
	++elements;
}

void List::remove(int number)
{
	//Sprawdza czy wartosc jest wartoscia pierwszego elementu
	if (start->value == number){
		element* temp = start;
		start = start->next;
		delete start;
		--elements;
		return;
	}
	//Szuka elementu i usuwa go
	if (start != NULL) {
		element* pointer = start;
		while (pointer->next != NULL) {
			//Zatrzymanie gdy znaleziono
			if ((pointer->next->value) == number) {
				//Usuniecie elementu i polaczenie kolejnych
				element* temp = pointer->next;
				pointer->next = pointer->next->next;
				delete temp;
				--elements;
				return;
			}
			pointer = pointer->next;
		}
		//Zwraca wyjatek, gdy nie znaleziono
		throw new std::invalid_argument("Brak podanego elementu w strukturze");
	}
}

void List::removeAll()
{
	//Usuwa elementy listy poruszajac sie po niej
	element* pointer = start;
	element* temp;
	while (pointer != NULL) {
		temp = pointer->next;
		delete pointer;
		pointer = temp;
	}
	start = NULL;
	last = NULL;
	elements = 0;
}

bool List::exist(void* i, int value)
{
	element* pointer = start;
	//Porusza sie po liscie, sprawdza czy wartosc jest rowna podanej wartosci
	while (pointer != NULL) {
		if ((pointer->value) == value) return true;
		pointer = pointer->next;
	}
	return false;
}

std::string List::toString(void* i)
{
	std::string out = "";
	element* pointer;
	if (i == NULL) pointer = start;
	else pointer = (element*)i;
	//Dopisuje wartosci kolejnych elementow listy
	while (pointer != NULL)
	{
		if(i==0) out += "[" + std::to_string(pointer->value) + "] -> ";
		pointer = pointer->next;
	}
	out += "NULL";
	return out;
}

int List::getLast()
{
	//Gdy lista istnieje -> zwraca wartosc ostatniego elementu
	if (elements) return last->value;
	return 0;
}
