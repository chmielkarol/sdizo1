#pragma once
#include "Data_struct.h"

class List :
	public Data_struct
{

struct element
{
	element* previous;
	int value;
	element* next;
};

public:
	List();
	~List();

	//Dodaje element na koncu listy
	void add(int value) override;

	//Dodaje element po elemencie o okreslonej wartosci
	void insert(int pos, int value) override;

	//Usuwa element o podanej wartosci
	void remove(int number) override;

	//Usuwa wszystkie elementy
	void removeAll() override;

	//Zwraca czy element o podanej wartosci wystepuje w liscie
	bool exist(void* i, int value) override;

	//Zwraca reprezentacje listy jako string
	std::string toString(void* i) override;

	//Zwraca ostatni element
	int getLast() override;

private:
	//Wska�nik na poczatek listy
	element* start;
	//Wska�nik na koniec listy
	element* last;
};