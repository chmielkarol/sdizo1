#include "stdafx.h"
#include "Data_struct.h"


Data_struct::Data_struct()
{
	elements = 0;
}


Data_struct::~Data_struct()
{
}

void Data_struct::add(int value)
{
}

void Data_struct::insert(int pos, int value)
{
}

void Data_struct::add_file(std::string path)
{
	//Obiekt pliku
	std::ifstream file;
	//Maska wyjatkow
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	//Otwiera plik
	file.open(path);
	//Sprawdza czy plik zostal otwarty poprawnie
	if (file.is_open()) {
		std::string line;
		getline(file, line);
		unsigned int length = std::stoi(line);
		//Dodaje kazda wartosc do struktury
		for (unsigned int i = 0; i < length; ++i) {
			getline(file, line);
			add(std::stoi(line));
		}
	}
	//Zamyka plik
	file.close();
}

void Data_struct::remove(int number)
{
}

void Data_struct::removeAll()
{
	elements = 0;
}

bool Data_struct::exist(void* i, int value)
{
	return false;
}

std::string Data_struct::toString(void* i)
{
	return std::string();
}

unsigned int Data_struct::getElements()
{
	return elements;
}

int Data_struct::getLast()
{
	return 0;
}
