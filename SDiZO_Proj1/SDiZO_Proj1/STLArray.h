#pragma once
#include "Data_struct.h"
#include <vector>

class STLArray :
	public Data_struct
{
public:
	STLArray();
	~STLArray();

	//Dodaje element do tablicy na koncu
	void add(int value) override;

	//Dodaje element do tablicy na wyznaczonym indeksie
	void insert(int pos, int value) override;

	//Usuwa element o podanej wartosci
	void remove(int number) override;

	//Usuwa wszystkie elementy
	void removeAll() override;

	//Zwraca czy element o podanej wartosci istnieje w tablicy
	bool exist(void* i, int value) override;

	//Zwraca reprezentacje tablicy jako string
	std::string toString(void* i) override;

	//Zwraca wskazanie na ostatni element
	int getLast() override;

private:
	//Obiekt tablicy STL
	std::vector<int> data;
};

