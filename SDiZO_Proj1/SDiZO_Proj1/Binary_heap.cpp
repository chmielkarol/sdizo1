#include "stdafx.h"
#include "Binary_heap.h"

Binary_heap::Binary_heap()
{
	root = NULL;
	last_leaf = NULL;
}

Binary_heap::~Binary_heap()
{
}

void Binary_heap::add(int value)
{
	++elements;
	bool end = false;
	//Gdy brak elementow -> tworzy kopiec
	if (last_leaf == NULL) {
		last_leaf = root = new element();
		last_leaf->parent = NULL;
		last_leaf->key = value;
		last_leaf->left = NULL;
		last_leaf->right = NULL;
		return;
	}
	//Poszukiwanie nowego ostatniego liscia
	while (last_leaf->parent != NULL) {
		if (last_leaf->parent->left == last_leaf) {
			last_leaf = last_leaf->parent;
			if (last_leaf->right == NULL) {
				//Dodanie elementu
				last_leaf->right = new element();
				last_leaf->right->parent = last_leaf;
				last_leaf = last_leaf->right;
			}
			else {
				//Dodanie elementu
				last_leaf = last_leaf->right;
				while (last_leaf->left != NULL) last_leaf = last_leaf->left;
				last_leaf->left = new element();
				last_leaf->left->parent = last_leaf;
				last_leaf = last_leaf->left;
			}
			end = true;
			break;
		}
		if(!end) last_leaf = last_leaf->parent;
	}
	if (!end) {
		//Dodanie elementu
		last_leaf = root;
		while (last_leaf->left != NULL) last_leaf = last_leaf->left;
		last_leaf->left = new element();
		last_leaf->left->parent = last_leaf;
		last_leaf = last_leaf->left;
	}
	//Przypisanie wartosci
	last_leaf->key = value;
	//Ustawienie wskazania na lewego syna
	last_leaf->left = NULL;
	//Ustawienie wskazania na prawego syna
	last_leaf->right = NULL;

	//Sprawdzenie warunku kopca
	element* pointer = last_leaf;
	while(pointer->parent !=NULL){
		if (pointer->key <= pointer->parent->key) return;
		int temp = pointer->parent->key;
		pointer->parent->key = pointer->key;
		pointer->key = temp;
		pointer = pointer->parent;
	}
}

//Funkcja dodaje element na koncu, wykorzystuje add()
void Binary_heap::insert(int pos, int value)
{
	add(value);
}

void Binary_heap::remove(int number)
{
	//Szuka element o podanym kluczu
	element* pointer = find(NULL, number);
	//Jesli nie znaleziono -> zglaszany wyjatek
	if (pointer == NULL) throw new std::invalid_argument("Nie ma takiego klucza w strukturze.");
	//Gdy istnieje tylko jeden element -> usuwa go
	if (last_leaf == root) {
		root = NULL;
		last_leaf = NULL;
		return;
	}
	//Przepisanie klucza z ostatniego liscia
	pointer->key = last_leaf->key;
	//Przywrocenie wlasciwosci kopca
	if (pointer->parent != NULL && pointer->parent->key < pointer->key)
		while (pointer->parent != NULL) {
			if (pointer->key <= pointer->parent->key) break;
			int temp = pointer->parent->key;
			pointer->parent->key = pointer->key;
			pointer->key = temp;
			pointer = pointer->parent;
		}
	else while (pointer->left != NULL) {
			if (pointer->key < pointer->left->key) {
				int temp = pointer->left->key;
				pointer->left->key = pointer->key;
				pointer->key = temp;
				pointer = pointer->left;
				continue;
				}
			if ((pointer->right != NULL) && (pointer->key < pointer->right->key)) {
				int temp = pointer->right->key;
				pointer->right->key = pointer->key;
				pointer->key = temp;
				pointer = pointer->right;
				continue;
			}
			break;
		 }
	//Przepisanie wskaznika na stary ostatni lisc
	element* previous = last_leaf;
	//Wyznaczenie nowego ostatniego liscia
	bool end = false;
	while (last_leaf->parent != NULL) {
		if (last_leaf->parent->right == last_leaf) {
			last_leaf = last_leaf->parent;
			end = true;
			break;
		}
		last_leaf = last_leaf ->parent;
	}
	//Usuniecie ostatniego liscia
	if (previous->parent->left == previous) previous->parent->left = NULL;
	else previous->parent->right = NULL;
	--elements;
	if (end) {
		last_leaf = last_leaf->left;
		while (last_leaf->right != NULL) last_leaf = last_leaf->right;
		return;
	}
	last_leaf = root;
	while (last_leaf->right != NULL) last_leaf = last_leaf->right;
}

void Binary_heap::remove(element* pointer)
{
	//Usuwa element i jego poddrzewa
	if (pointer->left != NULL) {
		remove(pointer->left);
		pointer->left = NULL;
	}
	if (pointer->right != NULL) {
		remove(pointer->right);
		pointer->right = NULL;
	}
	delete pointer;
}

void Binary_heap::removeAll()
{
	//Jesli drzewo istnieje usuwa go
	if (root != NULL) {
		remove(root);
		root = NULL;
		last_leaf = NULL;
		elements = 0;
	}
}

bool Binary_heap::exist(void* i, int value)
{
	element* pointer;
	//Ustawia wartosc poczatkowa
	if (i == NULL) {
		if (root == NULL) return false;
		pointer = root;
	}
	else pointer = (element*)i;
	//Jesli wartosc jest rowna wartosci elementu poczatkowego zwraca TRUE
	if (value == pointer->key) return true;
	//Jesli wartosc jest wieksza od wartosci elementu poczatkowego zwraca FALSE
	if (value > pointer->key) return false;
	//Sprawdza rekurencyjnie czy podany element istnieje
	return ((pointer->left != NULL && exist(pointer->left, value))
		|| (pointer->right != NULL && exist(pointer->right, value)));
}

std::string Binary_heap::toString(void* i)
{
	element* pointer;
	//Ustawia wartosc poczatkowa
	if (i == NULL) pointer = root;
	else pointer = (element*)i;
	if (pointer == NULL) return "";
	std::string out = "\nPOINT | P: ";
	//Dodaje wartosc ojca do opisu
	if (pointer->parent != NULL) out += std::to_string(pointer->parent->key);
	else out += "NULL";
	//Dodaje swoja wartosc do opisu
	out += (" | key: " + std::to_string(pointer->key));
	out += " | L: ";
	//Dodaje wartosc lewego syna do opisu
	if (pointer->left != NULL) out += std::to_string(pointer->left->key);
	else out += "NULL";
	out += " | R: ";
	//Dodaje wartosc prawego syna do opisu
	if (pointer->right != NULL) out += std::to_string(pointer->right->key);
	else out += "NULL";
	if (pointer->left != NULL) out += (toString(pointer->left));
	if (pointer->right != NULL) out += (toString(pointer->right));
	return out;
}

int Binary_heap::getLast()
{
	//Sprawdza czy kopiec istnieje
	if (root == NULL) return 0;
	element* pointer = root;
	//Porusza sie po prawych synach, az do konca
	if (root->right == NULL) return last_leaf->key;
	while (pointer->right != NULL) pointer = pointer->right;
	return pointer->key;
}

Binary_heap::element* Binary_heap::find(element * i, int value)
{
	element* pointer;
	//Ustawia wartosc poczatkowa
	if (i == NULL) pointer = root;
	else pointer = (element*)i;
	//Jesli wartosc jest rowna wartosci poczatkowej zwraca TRUE
	if (value == pointer->key) return pointer;
	//Jesli wartosc jest wieksza od wartosci poczatkowej zwraca FALSE
	if (value > pointer->key) return NULL;
	element* search;
	//Wykonuje rekurencyjnie poszukiwanie w lewym i prawym poddrzewie
	if (pointer->left != NULL) {
		search = find(pointer->left, value);
		if (search != NULL) return search;
	}
	if (pointer->right != NULL) {
		search = find(pointer->right, value);
		if (search != NULL) return search;
	}
	return NULL;
}
