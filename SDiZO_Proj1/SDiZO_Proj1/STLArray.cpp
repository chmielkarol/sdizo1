#include "stdafx.h"
#include "STLArray.h"


STLArray::STLArray()
{
}


STLArray::~STLArray()
{
	data.clear();
}

void STLArray::add(int value)
{
	//Dodaje element na koncu tablicy
	data.push_back(value);
}

void STLArray::insert(int pos, int value)
{
	//Wkleja element na pozycji okreslonej przez indeks
	data.insert(data.begin() + pos, value);
}

void STLArray::remove(int number)
{
	//Usuwa element o podanym indeksie
	data.erase(data.begin() + number);
}

void STLArray::removeAll()
{
	//Usuwa tablice
	data.clear();
}

bool STLArray::exist(void* i, int value)
{
	//Jesli znaleziono wartosc w tablicy zwraca TRUE
	return (std::find(data.begin(), data.end(), value) != data.end());
}

std::string STLArray::toString(void* i)
{
	std::string out = "[";
	//Dopisuje kolejne elementy tablicy
	for (unsigned int x = 0; x < data.size(); ++x) {
		out += (std::to_string(data[x]) + ",");
	}
	out.pop_back();
	out += "]";
	return out;
}

int STLArray::getLast()
{
	//Zwraca wartosc ostatniego elementu, wykorzystujac metode back()
	if (data.size()) return data.back();
	return 0;
}
