#pragma once
#include "Data_struct.h"
class RBTree :
	public Data_struct
{
	//Definicje stalych kolorow
	typedef bool color;
	const color RED = true;
	const color BLACK = false;

	//Struktura reprezentujaca element drzewa
	struct element
	{
		//Wskazanie na ojca
		element* parent;
		//Klucz
		int key;
		//Kolor
		color color;
		//Wskazanie na lewego syna
		element* left;
		//Wskazanie na prawego syna
		element* right;
	};

	//Staly element pusty
	element* const NIL = new element({ NULL, NULL, BLACK, NULL, NULL });

public:
	RBTree();
	~RBTree();

	//Dodaje element do drzewa
	void add(int value) override;

	//Dodaje element do drzewa
	void insert(int pos, int value) override;

	//Usuwa element o podanym kluczu
	void remove(int number) override;

	//Usuwa element, przyjmujac wskazanie na niego
	void remove(element* pointer);

	//Usuwa wszystkie elementy
	void removeAll() override;

	//Zwraca czy element o podanym kluczu wystepuje w drzewie
	bool exist(void* i, int value) override;

	//Zwraca reprezentacje drzewa jako string
	std::string toString(void* i) override;

	//Zwraca ostatni element
	int getLast() override;

private:
	//Zaczynajac od podanego elementu szuka podanego klucza i zwraca wskaznik na znaleziony element
	RBTree::element* find(element* i, int value);

	//Wskazanie na korzen
	element* root;

	//Usuwa element i jego poddrzewa
	void destroy(element* pointer);

	//Przywraca wlasnosci drzewa czerwono-czarnego po dodaniu elementu
	void restore_add(element* pointer);

	//Przywraca wlasnosci drzewa czerwono-czarnego po usunieciu elementu
	void restore_remove(element* pointer);

	//Rotacja w lewo
	void rot_L(element* pointer);

	//Rotacja w prawo
	void rot_R(element* pointer);

	//Zwraca poprzednik elementu
	element* predecessor(element* pointer);
	
	//Zwraca nastepnik elementu
	element* successor(element* pointer);
};

