#pragma once
#include "Data_struct.h"
#include "Array.h"

class Binary_heap :
	public Data_struct
{

//Struktura reprezentujaca element kopca
struct element
{
	//Wskazanie na ojca
	element* parent;
	//Kluczz
	int key;
	//Wskazanie na lewego syna
	element* left;
	//Wskazanie na prawego syna
	element* right;
};

public:
	Binary_heap();
	~Binary_heap();

	//Dodaje element do kopca
	void add(int value) override;

	//Dodaje element do kopca
	void insert(int pos, int value) override;

	//Usuwa element o podanym kluczu
	void remove(int number) override;

	//USuwa element przyjmujac wskazanie na niego
	void remove(element* pointer);

	//Usuwa wszystkie elementy
	void removeAll() override;

	//Zwraca czy element o podanym kluczu istnieje
	bool exist(void* i, int value) override;

	//Zwraca odwzorowanie kopca jako string
	std::string toString(void* i) override;

	//Zwraca ostatni element
	int getLast() override;

private:
	//Zaczynajac od podanego elementu szuka podanego klucza i zwraca wskaznik na znaleziony element
	Binary_heap::element* find(element* i, int value);
	//Wska�nik na korzen
	element* root;
	//Wska�nik na ostatnio dodany lisc
	element* last_leaf;
};

