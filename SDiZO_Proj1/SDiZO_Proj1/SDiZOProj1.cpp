﻿#include "stdafx.h"
#include "Data_struct.h"
#include "Array.h"
#include "List.h"
#include "Binary_heap.h"
#include "STLArray.h"
#include "STLList.h"
#include "RBTree.h"

//Funkcja wyboru opcji z podanej puli opcji
char getOption(std::string text, char min, char max) {
	char value;
	while (true) {
		std::cin.clear();
		system("cls");
		std::cout << text << std::endl;
		std::cout << "Wybor: ";
		std::cin >> value;
		if (std::cin.good() && value >= min && value <= max) return value;
		std::cout << "Podano nieprawidlowy numer.\n";
		system("pause");
	}
}

//Funkcja pobiera nazwe pliku od uzytkownika
std::string getPath() {
	std::string path;
	while (true) {
		std::cin.clear();
		system("cls");
		std::cout << "Podaj sciezke do pliku: ";
		std::cin >> path;
		if (std::cin.good() && !path.empty()) return path;
		std::cout << "Podano nieprawidlowo sciezke.\n";
		system("pause");
	}
}

//Funkcja pobiera wartosc liczbowa od uzytkownika
int getValue(std::string text) {
	int value;
	while (true) {
		std::cin.clear();
		system("cls");
		std::cout << text << std::endl;
		std::cout << "Wartosc: ";
		std::cin >> value;
		if (std::cin.good()) return value;
		std::cout << "Wpisz wartosc poprawnie.\n";
		system("pause");
	}
}

//Procedura startujaca stoper
LARGE_INTEGER startTimer()
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}

//Procedura zatrzymujaca stoper
LARGE_INTEGER endTimer()
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}

//Funkcja losujaca liczbe w rozny sposob, w zaleznosci od wybranego trybu
int rand(int mode, std::asdasdasdasdasdasdasdasdasdssssaddassdadassdaadsdsadasdsaadsadsadsdasadsadsadsasdsdaadssddsasdasdasddefault_random_engine* e1, std::uniform_int_distribution<>* dist) {
	switch (mode) {
		case 0:
			return (*dist)(*e1);
		case 1:
			return std::rand() - RAND_MAX/2;
		default:
			return std::rand();
	}
}

//Oblicza czasy operacji na struktury
void measure_time(Data_struct* database) {

	const std::type_info& type_info = typeid(*database);

	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	double add, find, remove;
	int value;
	int position = 0;

	std::random_device rd;
	std::default_random_engine e1(rd());
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dist(-INT_MAX, INT_MAX);
	srand((int)time(NULL));

	//Tworzy pliki z wynikami i otwiera je
	std::ofstream file_add;
	file_add.open("resultsADD.txt", std::ios::out);
	if (!file_add.is_open()) throw std::exception("Blad oczytu");
	std::ofstream file_remove;
	file_remove.open("resultsREMOVE.txt", std::ios::out);
	if (!file_remove.is_open()) throw std::exception("Blad oczytu");
	std::ofstream file_find;
	file_find.open("resultsFIND.txt", std::ios::out);
	if (!file_find.is_open()) throw std::exception("Blad oczytu");

	system("cls");
	std::cout << "POMIARY CZASU";
	int size = getValue("Podaj liczbe elementow losowanej populacji.");
	int range = size/getValue("Podaj ilosc przedzialow");
	std::cout << "###############\n";
	std::cout << "1. Wczytywanie\n";
	//Sprawdza czasy dla roznych rozmiarow struktur
	for (int i = 0; i <= size; i+=range) {
		std::cout << "\nPopulacja: " << i << std::endl;
		file_add << i;
		file_remove << i;
		file_find << i;
		//Sprawdza czasy dla roznych wartosci przechowywanych w strukturach
		for (int m = 0; m < 3; ++m) {
			switch (m) {
			case 0:
				std::cout << "\n  Tryb INT_MAX SIGNED ~[-2 500 000 000 - 2 500 000 000]\n";
				break;
			case 1:
				std::cout << "\n  Tryb RAND_MAX SIGNED [-16 384 - 16 384]\n";
				break;
			case 2:
				std::cout << "\n  Tryb RAND_MAX UNSIGNED [0-32 767]\n";
				break;
			}
			add = 0;
			remove = 0;
			find = 0;
			for (int j = 0; j < 100; ++j) {
				database->removeAll();
				for (int k = 0; k < i; ++k) database->add(rand(m, &e1, &dist));

				//Wstawianie

				value = rand(m, &e1, &dist);
				if (type_info == typeid(Array) || type_info == typeid(STLArray)) position = 0;
				else if (type_info == typeid(List) || type_info == typeid(STLList)) position = database->getLast();

				performanceCountStart = startTimer();
				database->insert(position, value);
				performanceCountEnd = endTimer();

				add += (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart);

				//Usuwanie

				if (type_info == typeid(Array) || type_info == typeid(STLArray)) value = 0;
				else if (type_info == typeid(Binary_heap)) value = database->getLast();

				performanceCountStart = startTimer();
				database->remove(value);
				performanceCountEnd = endTimer();

				remove += (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart);

				//Wyszukiwanie

				if (type_info == typeid(Binary_heap)) value = database->getLast();
				else value = rand(m, &e1, &dist);

				performanceCountStart = startTimer();
				database->exist(NULL, value);
				performanceCountEnd = endTimer();

				find += (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart);
			}
			//Oblicza srednia ze 100 wynikow
			std::cout << "		Dodawanie: " << add / 100 << "   Usuwanie: " << remove / 100 << "   Wyszukiwanie: "
				<< find / 100 << std::endl;
			//Zapisuje wyniki do plikow
			file_add << " " << add / 100;
			file_remove << " " << remove / 100;
			file_find << " " << find / 100;
		}
		file_add << std::endl;
		file_remove << std::endl;
		file_find << std::endl;
	}
	//Zamyka pliki
	file_add.close();
	file_remove.close();
	file_find.close();
}

int main()
{
	Data_struct* data = nullptr;
	bool result;
	int value;

	std::cout << "Karol Chmiel 235035\n";
	std::cout << "SDiZO - Projekt 1\n";
	std::cout << "--------------------\n";
	system("pause");

	//Umozliwia wybor struktury danych
	std::string path;
	switch (getOption(std::string("Wybierz strukture danych:\n")
		+ "1. Tablica\n"
		+ "2. Lista\n"
		+ "3. Kopiec binarny\n"
		+ "4. Drzewo czerwono-czarne\n"
		+ "5. Tablica (z biblioteki STL)\n"
		+ "6. Lista (z biblioteki STL)", '1', '6s')) {
		case '1':
			data = new Array();
			break;
		case '2':
			data = new List();
			break;
		case '3':
			data = new Binary_heap();
			break;
		case '4':
			data = new RBTree();
			break;
		case '5':
			data = new STLArray();
			break;
		case '6':
			data = new STLList();
	}

	//Pobiera informacje o typie struktury
	const std::type_info& type_info = typeid(*data);

	//Umozliwia wybor operacji na strukturze
	while (true) {
		switch (getOption(std::string("Operacje:\n")
			+ "1. Zbuduj z pliku\n"
			+ "2. Usun\n"
			+ "3. Dodaj\n"
			+ "4. Znajdz\n"
			+ "5. Wyswietl\n"
			+ "6. POMIAR CZASU\n"
			+ "7. Wyjdz", '1', '7')) {
		case '1':
			//Zbudowanie z pliku
			try {
				data->removeAll();
				data->add_file(getPath());
			}
			catch (std::ifstream::failure e) {
				std::cout << "\nWystapil blad (" << e.what() << ")";
			}
			break;
		case '2':
			//Usuniecie elementu
			try {
				if (type_info == typeid(Array)) data->remove(getValue("Podaj indeks elementu do usuniecia [0,1,2...]"));
				else if (type_info == typeid(List)) data->remove(getValue("Podaj wartosc elementu do usuniecia"));
				else if (type_info == typeid(Binary_heap)) data->remove(getValue("Podaj wartosc elementu do usuniecia"));
				else if (type_info == typeid(RBTree)) data->remove(getValue("Podaj wartosc elementu do usuniecia"));
				else if (type_info == typeid(STLArray)) data->remove(getValue("Podaj indeks elementu do usuniecia [0,1,2...]"));
				else data->remove(getValue("Podaj wartosc elementu do usuniecia"));
			}
			catch (...) {
				std::cout << "\nWystapil blad";
			}
			break;
		case '3':
			//Dodanie elementu
			try {
				if (type_info == typeid(Array)) data->insert(getValue("Podaj indeks pod ktorym wstawic element[0,1,2...]"), getValue("Podaj wartosc elementu do wstawienia"));
				else if (type_info == typeid(List)) data->insert(getValue("Podaj wartosc elementu za ktorym dokonac wstawienia"), getValue("Podaj wartosc elementu do wstawienia"));
				else if (type_info == typeid(Binary_heap)) data->add(getValue("Podaj wartosc elementu do wstawienia"));
				else if (type_info == typeid(RBTree)) data->add(getValue("Podaj wartosc elementu do wstawienia"));
				else if (type_info == typeid(STLArray)) data->insert(getValue("Podaj indeks pod ktorym wstawic element[0,1,2...]"), getValue("Podaj wartosc elementu do wstawienia"));
				else data->insert(getValue("Podaj wartosc elementu za ktorym dokonac wstawienia"), getValue("Podaj wartosc elementu do wstawienia"));
			}
			catch (...) {
				std::cout << "\nWystapil blad";
			}
			break;
		case '4':
			//Wyszukanie elementu
			value = getValue("Podaj wartosc elementu do wyszukania");
			result = data->exist(NULL, value);
			if (result) std::cout << "Wynik: ZNALEZIONO ELEMENT";
			else std::cout << "Wynik: NIE ZNALEZIONO ELEMENTU";
			break;
		case '5':
			//Wyswietlenie struktury
			system("cls");
			std::cout << "WIDOK STRUKTURY\n" << data->toString(NULL);
			break;
		case '6':
			//Pomiar czasow operacji dla struktury
			measure_time(data);
			break;
		case '7':
			//Zakonczenie programu
			return 0;
		}
		std::cout << std::endl;
		system("pause");
	}
}

